﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebDulich.Models;
using System.IO;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;
using PagedList;
using PagedList.Mvc;
using System.Text;
using System.Text.RegularExpressions;
using WebDulich.App_Start;


namespace WebDulich.Areas.Admin.Controllers
{

    public class AdminManagerController : Controller
    {
        DULICHEntities1 data = new DULICHEntities1();
        
        // GET: Admin
       
        public ActionResult Index()
        {
            
            ADMIN ad = (ADMIN)Session["Admin"];
            if(ad == null)
            {
                return RedirectToAction("Index", "Home");
            }
            THONGKEWEB tk_web = data.THONGKEWEBs.SingleOrDefault(n => n.ID == 1);
            
            ViewBag.USER = tk_web.TOTAL_USER;
            ViewBag.BAIVIET = tk_web.TOTAL_BAIVIET;
            ViewBag.ONL = tk_web.TOTAL_ONL;
            ViewBag.TINTUC = tk_web.TOTAL_TINTUC;
            return View();
        }

        public ActionResult ListPost(int? page)
        {
            ADMIN ad = (ADMIN)Session["Admin"];
            if (ad == null)
            {
                return RedirectToAction("Index", "Home");
            }
            int pageNumber = (page ?? 1);
            int pageSize = 10;
            // return View(data.TINTUCs.ToList());
            return View(data.TINTUCs.ToList().OrderBy(n => n.ID).ToPagedList(pageNumber, pageSize));
        }

        public ActionResult SearchPost(string sTuKhoa, int? page)
        {
            if (Request.HttpMethod != "GET")
            {
                page = 1;
            }
            int Pagesize = 9;
            int PageNumber = (page ?? 1);

            string TuKhoa = sTuKhoa;
            TuKhoa = TuKhoa.GenerateSlug();

            var tin = data.TINTUCs.Where(n => n.TUKHOA.Contains(TuKhoa));
            ViewBag.TuKhoa = sTuKhoa;
            return View(tin.OrderBy(n => n.TENTIN).ToPagedList(PageNumber, Pagesize));

        }
        [HttpPost]
        public ActionResult SearchPost(string sTuKhoa)
        {
            return RedirectToAction("SearchPost", new { @sTuKhoa = sTuKhoa });

        }

        public ActionResult CreatePost()
        {
            ADMIN ad = (ADMIN)Session["Admin"];
            if (ad == null)
            {
                return RedirectToAction("Index", "Home");
            }
            var listCategory = from s in data.LOAITINs
                               select new { s.ID, s.TENLOAI };

            var list = listCategory.ToList().Select(c => new SelectListItem
            {
                Text = c.TENLOAI,
                Value = c.ID.ToString(),
                Selected = (c.ID == 1)
            }).ToList();

            ViewBag.listCate = list;
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreatePost([Bind(Include = "TENTIN, MOTA, NGAYDANG, ID_LOAI")] TINTUC tin, FormCollection collection)
        {
            THONGKEWEB tk_web = data.THONGKEWEBs.SingleOrDefault(n => n.ID == 1);
            var noidung = collection["noidung"];
            var hinh = collection["hinh"];
            
            try
            {
                if (ModelState.IsValid)
                {
                    tin.TUKHOA = tin.TENTIN;
                    tin.TUKHOA = tin.TUKHOA.GenerateSlug();
                    tin.NGAYDANG = DateTime.Now;
                    tin.HINHANH = hinh;
                    tin.NOIDUNG = noidung;
                    data.TINTUCs.Add(tin);
                    tk_web.TOTAL_TINTUC++;
                    data.SaveChanges();
                }
            }
            catch (RetryLimitExceededException)
            {
                ModelState.AddModelError("", "Có lỗi xảy ra");
            }
            return RedirectToAction("ListPost");
        }

        public ActionResult DetailPost(int id)
        {
            ADMIN ad = (ADMIN)Session["Admin"];
            if (ad == null)
            {
                return RedirectToAction("Index", "Home");
            }
            TINTUC tin = data.TINTUCs.SingleOrDefault(n => n.ID == id);
            ViewBag.Matin = tin.ID;
            if (tin == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            return View(tin);
        }

        public ActionResult DeletePost(int id)
        {
            ADMIN ad = (ADMIN)Session["Admin"];
            if (ad == null)
            {
                return RedirectToAction("Index", "Home");
            }
            TINTUC tin = data.TINTUCs.SingleOrDefault(n => n.ID == id);
            ViewBag.Matin = tin.ID;
            if (tin == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            return View(tin);
        }

        [HttpPost, ActionName("DeletePost")]
        public ActionResult DeletePost(int id, FormCollection collection)
        {
            THONGKEWEB tk_web = data.THONGKEWEBs.SingleOrDefault(n => n.ID == 1);
            TINTUC tin = data.TINTUCs.SingleOrDefault(n => n.ID == id);
            ViewBag.Matin = tin.ID;
            if (tin == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            data.TINTUCs.Remove(tin);
            tk_web.TOTAL_TINTUC--;
            data.SaveChanges();
            return RedirectToAction("ListPost");
        }

        public ActionResult EditPost(int id)
        {
            ADMIN ad = (ADMIN)Session["Admin"];
            if (ad == null)
            {
                return RedirectToAction("Index", "Home");
            }
            TINTUC tin = data.TINTUCs.SingleOrDefault(n => n.ID == id);
            ViewBag.Matin = tin.ID;
            if (tin == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            var listCategory = from s in data.LOAITINs
                               select new { s.ID, s.TENLOAI };

            var list = listCategory.ToList().Select(c => new SelectListItem
            {
                Text = c.TENLOAI,
                Value = c.ID.ToString(),
                Selected = (c.ID == 1)
            }).ToList();

            ViewBag.listCate = list;
            return View(tin);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditPost(int id, FormCollection collection)
        {
            TINTUC tin = data.TINTUCs.SingleOrDefault(n => n.ID == id);
            var noidung = collection["noidung"];
            var hinh = collection["hinh"];
            var tentin = collection["TENTIN"];
            var mota = collection["MOTA"];
            var loaitin = int.Parse(collection["ID_LOAI"]);
            try
            {
                if (ModelState.IsValid)
                {
                    tin.ID_LOAI = loaitin;
                    tin.TENTIN = tentin;
                    tin.TUKHOA = tin.TENTIN;
                    tin.TUKHOA = tin.TUKHOA.GenerateSlug();
                    tin.MOTA = mota;
                    tin.HINHANH = hinh;
                    tin.NOIDUNG = noidung;
                    data.Entry(tin).State = EntityState.Modified;
                    data.SaveChanges();
                }
            }
            catch (RetryLimitExceededException)
            {
                ModelState.AddModelError("", "Có lỗi xảy ra");
            }
            return RedirectToAction("ListPost");
        }

        // Quản lý người dùng
        public ActionResult ListUser(int? page)
        {
            ADMIN ad = (ADMIN)Session["Admin"];
            if (ad == null)
            {
                return RedirectToAction("Index", "Home");
            }
            int pageNumber = (page ?? 1);
            int pageSize = 7;
            // return View(data.TINTUCs.ToList());
            return View(data.NGUOIDUNGs.ToList().OrderBy(n => n.ID).ToPagedList(pageNumber, pageSize));
        }

        public ActionResult SearchUser(string sTuKhoa, int? page)
        {
            if (Request.HttpMethod != "GET")
            {
                page = 1;
            }
            int Pagesize = 9;
            int PageNumber = (page ?? 1);

            var us = data.NGUOIDUNGs.Where(n => n.TENDN.Contains(sTuKhoa));
            ViewBag.TuKhoa = sTuKhoa;
            return View(us.OrderBy(n => n.TENDN).ToPagedList(PageNumber, Pagesize));

        }
        [HttpPost]
        public ActionResult SearchUser(string sTuKhoa)
        {
            return RedirectToAction("SearchUser", new { @sTuKhoa = sTuKhoa });

        }

        public ActionResult DetailUser(int id)
        {
            ADMIN ad = (ADMIN)Session["Admin"];
            if (ad == null)
            {
                return RedirectToAction("Index", "Home");
            }
            NGUOIDUNG nd = data.NGUOIDUNGs.SingleOrDefault(n => n.ID == id);
            ViewBag.Mand = nd.ID;
            if (nd == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            return View(nd);
        }

        public ActionResult DeleteUser(int id)
        {
            ADMIN ad = (ADMIN)Session["Admin"];
            if (ad == null)
            {
                return RedirectToAction("Index", "Home");
            }
            NGUOIDUNG nd = data.NGUOIDUNGs.SingleOrDefault(n => n.ID == id);
            ViewBag.Mand = nd.ID;
            if (nd == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            return View(nd);
        }

        [HttpPost, ActionName("DeleteUser")]
        public ActionResult DeleteUser(int id, FormCollection collection)
        {
            THONGKEWEB tk_web = data.THONGKEWEBs.SingleOrDefault(n => n.ID == 1);
            NGUOIDUNG nd = data.NGUOIDUNGs.SingleOrDefault(n => n.ID == id);
            ViewBag.Mand = nd.ID;
            if (nd == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            data.NGUOIDUNGs.Remove(nd);
            tk_web.TOTAL_USER--;
            data.SaveChanges();
            return RedirectToAction("ListUser");
        }

        public ActionResult EditUser(int id)
        {
            ADMIN ad = (ADMIN)Session["Admin"];
            if (ad == null)
            {
                return RedirectToAction("Index", "Home");
            }
            NGUOIDUNG nd = data.NGUOIDUNGs.SingleOrDefault(n => n.ID == id);
            ViewBag.Mand = nd.ID;
            if (nd == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            return View(nd);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditUser(NGUOIDUNG nd, FormCollection collection)
        {
            if (TryUpdateModel(nd, "", new string[] { "TEN, TENDN, EMAIL, PASS, SODT" }))
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        data.Entry(nd).State = EntityState.Modified;
                        data.SaveChanges();
                    }
                }
                catch (RetryLimitExceededException)
                {
                    ModelState.AddModelError("", "Có lỗi xảy ra");
                }
            }

            return RedirectToAction("ListUser");
        }

        public ActionResult ListUserFb(int? page)
        {
            ADMIN ad = (ADMIN)Session["Admin"];
            if (ad == null)
            {
                return RedirectToAction("Index", "Home");
            }
            int pageNumber = (page ?? 1);
            int pageSize = 7;
            // return View(data.TINTUCs.ToList());
            return View(data.NGUOIDUNGFBs.ToList().OrderBy(n => n.ID).ToPagedList(pageNumber, pageSize));
        }

        public ActionResult SearchUserFb(string sTuKhoa, int? page)
        {
            if (Request.HttpMethod != "GET")
            {
                page = 1;
            }
            int Pagesize = 9;
            int PageNumber = (page ?? 1);

            var usf = data.NGUOIDUNGFBs.Where(n => n.TENDN.Contains(sTuKhoa));
            ViewBag.TuKhoa = sTuKhoa;
            return View(usf.OrderBy(n => n.TENDN).ToPagedList(PageNumber, Pagesize));

        }
        [HttpPost]
        public ActionResult SearchUserFb(string sTuKhoa)
        {
            return RedirectToAction("SearchUserFb", new { @sTuKhoa = sTuKhoa });

        }

        public ActionResult DetailUserFb(string id)
        {
            ADMIN ad = (ADMIN)Session["Admin"];
            if (ad == null)
            {
                return RedirectToAction("Index", "Home");
            }
            NGUOIDUNGFB ndfb = data.NGUOIDUNGFBs.SingleOrDefault(n => n.ID == id);
            ViewBag.Mand = ndfb.ID;
            if (ndfb == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            return View(ndfb);
        }

        public ActionResult DeleteUserFb(string id)
        {
            ADMIN ad = (ADMIN)Session["Admin"];
            if (ad == null)
            {
                return RedirectToAction("Index", "Home");
            }
            NGUOIDUNGFB ndfb = data.NGUOIDUNGFBs.SingleOrDefault(n => n.ID == id);
            ViewBag.Mand = ndfb.ID;
            if (ndfb == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            return View(ndfb);
        }

        [HttpPost, ActionName("DeleteUserFb")]
        public ActionResult DeleteUserFb(string id, FormCollection collection)
        {
            THONGKEWEB tk_web = data.THONGKEWEBs.SingleOrDefault(n => n.ID == 1);
            NGUOIDUNGFB ndfb = data.NGUOIDUNGFBs.SingleOrDefault(n => n.ID == id);
            ViewBag.Mand = ndfb.ID;
            if (ndfb == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            data.NGUOIDUNGFBs.Remove(ndfb);
            tk_web.TOTAL_USER--;
            data.SaveChanges();
            return RedirectToAction("ListUserFb");
        }

        

        // Quản lý bài viết từ người dùng

        public ActionResult ListWrite(int? page)
        {
            ADMIN ad = (ADMIN)Session["Admin"];
            if (ad == null)
            {
                return RedirectToAction("Index", "Home");
            }
            int pageNumber = (page ?? 1);
            int pageSize = 7;
            // return View(data.TINTUCs.ToList());
            return View(data.BAIVIETs.ToList().OrderBy(n => n.ID).ToPagedList(pageNumber, pageSize));
        }

        public ActionResult SearchWrite(string sTuKhoa, int? page)
        {
            if (Request.HttpMethod != "GET")
            {
                page = 1;
            }
            int Pagesize = 9;
            int PageNumber = (page ?? 1);

            var bv = data.BAIVIETs.Where(n => n.TIEUDE.Contains(sTuKhoa));
            ViewBag.TuKhoa = sTuKhoa;
            return View(bv.OrderBy(n => n.TIEUDE).ToPagedList(PageNumber, Pagesize));

        }
        [HttpPost]
        public ActionResult SearchWrite(string sTuKhoa)
        {
            return RedirectToAction("SearchWrite", new { @sTuKhoa = sTuKhoa });

        }

        public ActionResult DetailWrite(int id)
        {
            ADMIN ad = (ADMIN)Session["Admin"];
            if (ad == null)
            {
                return RedirectToAction("Index", "Home");
            }
            BAIVIET bv = data.BAIVIETs.SingleOrDefault(n => n.ID == id);
            ViewBag.Mabv = bv.ID;
            if (bv == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            return View(bv);
        }

        public ActionResult AcceptWrite(int id)
        {
            ADMIN ad = (ADMIN)Session["Admin"];
            if (ad == null)
            {
                return RedirectToAction("Index", "Home");
            }
            BAIVIET bv = data.BAIVIETs.SingleOrDefault(n => n.ID == id);
            ViewBag.Mabv = bv.ID;
            if (bv == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            var listCategory = from s in data.LOAITINs
                               select new { s.ID, s.TENLOAI };

            var list = listCategory.ToList().Select(c => new SelectListItem
            {
                Text = c.TENLOAI,
                Value = c.ID.ToString(),
                Selected = (c.ID == 1)
            }).ToList();

            ViewBag.listCate = list;
            return View(bv);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AcceptWrite(int id, FormCollection collection)
        {
            TINTUC tin= new TINTUC();
            THONGKEWEB tk_web = data.THONGKEWEBs.SingleOrDefault(n => n.ID == 1);
            BAIVIET bv = data.BAIVIETs.SingleOrDefault(n => n.ID == id);
            var noidung = collection["noidung"];
            var hinh = collection["hinh"];
            var loaitin = int.Parse(collection["id_loai"]);
            var tieude = collection["tieude"];
            var mota = collection["mota"];

            try
            {
                    tin.ID_LOAI = loaitin;
                    tin.TENTIN = tieude;
                    tin.TUKHOA = tieude;
                    tin.TUKHOA = tin.TUKHOA.GenerateSlug();
                    tin.ID_USER = bv.ID_USER;
                    tin.ID_USERFB = bv.ID_USERFB;
                    tin.MOTA = mota;
                    tin.NGAYDANG = DateTime.Now;
                    tin.HINHANH = hinh;
                    tin.NOIDUNG = noidung;
                    data.TINTUCs.Add(tin);
                    tk_web.TOTAL_TINTUC++;
                    data.SaveChanges();
                return RedirectToAction("ListPost");
            }
            catch (RetryLimitExceededException)
            {
                ModelState.AddModelError("", "Có lỗi xảy ra");
            }
            return RedirectToAction("ListPost");
        }

        public ActionResult DeleteWrite(int id)
        {
            ADMIN ad = (ADMIN)Session["Admin"];
            if (ad == null)
            {
                return RedirectToAction("Index", "Home");
            }
            BAIVIET bv = data.BAIVIETs.SingleOrDefault(n => n.ID == id);
            ViewBag.Mabv = bv.ID;
            if (bv == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            return View(bv);
        }

        [HttpPost, ActionName("DeleteWrite")]
        public ActionResult DeleteWrite(int id, FormCollection collection)
        {
            THONGKEWEB tk_web = data.THONGKEWEBs.SingleOrDefault(n => n.ID == 1);
            BAIVIET bv = data.BAIVIETs.SingleOrDefault(n => n.ID == id);
            ViewBag.Mabv = bv.ID;
            if (bv == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            data.BAIVIETs.Remove(bv);
            tk_web.TOTAL_BAIVIET--;
            data.SaveChanges();
            return RedirectToAction("ListWrite");
        }

        public ActionResult EditWrite(int id)
        {
            ADMIN ad = (ADMIN)Session["Admin"];
            if (ad == null)
            {
                return RedirectToAction("Index", "Home");
            }
            BAIVIET bv = data.BAIVIETs.SingleOrDefault(n => n.ID == id);
            ViewBag.Mabv = bv.ID;
            if (bv == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            var listCategory = from s in data.LOAITINs
                               select new { s.ID, s.TENLOAI };

            var list = listCategory.ToList().Select(c => new SelectListItem
            {
                Text = c.TENLOAI,
                Value = c.ID.ToString(),
                Selected = (c.ID == 1)
            }).ToList();

              ViewBag.listCate = list;
            return View(bv);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditWrite(int id, FormCollection collection)
        {
            BAIVIET bv = data.BAIVIETs.SingleOrDefault(n => n.ID == id);
            var noidung = collection["noidung"];
            var hinh = collection["hinh"];
            var loaitin = int.Parse(collection["id_loai"]);
            var tieude = collection["tieude"];
            var mota = collection["mota"];

            if (TryUpdateModel(bv, "", new string[] { "TENTIN, MOTA, NGAYDANG, ID_LOAI" }))
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        bv.ID_LOAI = loaitin;
                        bv.TIEUDE = tieude;
                        bv.MOTA = mota;
                        bv.NOIDUNG = noidung;
                        bv.ANHBIA = hinh;
                        data.Entry(bv).State = EntityState.Modified;
                        data.SaveChanges();
                    }
                    catch (RetryLimitExceededException)
                    {
                        ModelState.AddModelError("", "Có lỗi xảy ra");
                    }
                }
               
            }
            return RedirectToAction("ListWrite");
        }


        // Quản lý lời nhắn phản hồi từ người dùng

        public ActionResult ListMessage(int? page)
        {
            ADMIN ad = (ADMIN)Session["Admin"];
            if (ad == null)
            {
                return RedirectToAction("Index", "Home");
            }
            int pageNumber = (page ?? 1);
            int pageSize = 7;
            // return View(data.TINTUCs.ToList());
            return View(data.TINNHANs.ToList().OrderBy(n => n.ID).ToPagedList(pageNumber, pageSize));
        }

        public ActionResult SearchMessage(string sTuKhoa, int? page)
        {
            if (Request.HttpMethod != "GET")
            {
                page = 1;
            }
            int Pagesize = 9;
            int PageNumber = (page ?? 1);

            var tn = data.TINNHANs.Where(n => n.HOTEN.Contains(sTuKhoa));
            ViewBag.TuKhoa = sTuKhoa;
            return View(tn.OrderBy(n => n.HOTEN).ToPagedList(PageNumber, Pagesize));

        }
        [HttpPost]
        public ActionResult SearchMessage(string sTuKhoa)
        {
            return RedirectToAction("SearchMessage", new { @sTuKhoa = sTuKhoa });

        }

        public ActionResult DetailMessage(int id)
        {
            ADMIN ad = (ADMIN)Session["Admin"];
            if (ad == null)
            {
                return RedirectToAction("Index", "Home");
            }
            TINNHAN tin = data.TINNHANs.SingleOrDefault(n => n.ID == id);
            ViewBag.Matn = tin.ID;
            if (tin == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            return View(tin);
        }

        public ActionResult DeleteMessage(int id)
        {
            ADMIN ad = (ADMIN)Session["Admin"];
            if (ad == null)
            {
                return RedirectToAction("Index", "Home");
            }
            TINNHAN tin = data.TINNHANs.SingleOrDefault(n => n.ID == id);
            ViewBag.Matn = tin.ID;
            if (tin == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            return View(tin);
        }

        [HttpPost, ActionName("DeleteMessage")]
        public ActionResult DeleteMessage(int id, FormCollection collection)
        {
            TINNHAN tin = data.TINNHANs.SingleOrDefault(n => n.ID == id);
            ViewBag.Matn = tin.ID;
            if (tin == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            data.TINNHANs.Remove(tin);
            data.SaveChanges();
            return RedirectToAction("ListMessage");
        }

        public ActionResult EditMessage(int id)
        {
            ADMIN ad = (ADMIN)Session["Admin"];
            if (ad == null)
            {
                return RedirectToAction("Index", "Home");
            }
            TINNHAN tin = data.TINNHANs.SingleOrDefault(n => n.ID == id);
            ViewBag.Matn = tin.ID;
            if (tin == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            return View(tin);
        }
        
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditMessage(TINNHAN tin, HttpPostedFileBase hinh, FormCollection collection)
        {
            var noidung = collection["noidung"];
            if (TryUpdateModel(tin, "", new string[] { "TENTIN, MOTA, NGAYDANG, ID_LOAI" }))
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        tin.NOIDUNG = noidung;
                        data.Entry(tin).State = EntityState.Modified;
                        data.SaveChanges();
                    }
                }
                catch (RetryLimitExceededException)
                {
                    ModelState.AddModelError("", "Có lỗi xảy ra");
                }
            }
            return RedirectToAction("ListMessage");
        }
       
    }
}