﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebDulich.Startup))]
namespace WebDulich
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
