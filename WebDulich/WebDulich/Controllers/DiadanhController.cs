﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebDulich.Models;
using PagedList;
using PagedList.Mvc;
using System.Xml;
using System.IO;

namespace WebDulich.Controllers
{
    public class DiadanhController : Controller
    {
        DULICHEntities1 data = new DULICHEntities1();
        // GET: Diadanh

        public ActionResult Mienbac(int ?page)
        {
            int pageNumber = (page ?? 1);
            int pageSize = 32;
            // return View(data.TINTUCs.ToList());
            return View(data.TINTUCs.ToList().OrderBy(n => n.ID).ToPagedList(pageNumber, pageSize));
        }


        public ActionResult Mientrung(int ?page)
        {
            int pageNumber = (page ?? 1);
            int pageSize = 32;
            // return View(data.TINTUCs.ToList());
            return View(data.TINTUCs.ToList().OrderBy(n => n.ID).ToPagedList(pageNumber, pageSize));
        }

        public ActionResult Miennam(int ?page)
        {
            int pageNumber = (page ?? 1);
            int pageSize = 32;
            // return View(data.TINTUCs.ToList());
            return View(data.TINTUCs.ToList().OrderBy(n => n.ID).ToPagedList(pageNumber, pageSize));
        }

        public ActionResult Mientay(int ?page)
        {
            int pageNumber = (page ?? 1);
            int pageSize = 32;
            // return View(data.TINTUCs.ToList());
            return View(data.TINTUCs.ToList().OrderBy(n => n.ID).ToPagedList(pageNumber, pageSize));
        }

        public ActionResult Taynguyen(int ?page)
        {
            int pageNumber = (page ?? 1);
            int pageSize = 32;
            // return View(data.TINTUCs.ToList());
            return View(data.TINTUCs.ToList().OrderBy(n => n.ID).ToPagedList(pageNumber, pageSize));
        }

        public ActionResult Biendao(int ?page)
        {
            int pageNumber = (page ?? 1);
            int pageSize = 32;
            // return View(data.TINTUCs.ToList());
            return View(data.TINTUCs.ToList().OrderBy(n => n.ID).ToPagedList(pageNumber, pageSize));
        }

        public ActionResult Chitiet(int id)
        {
            var tin = from x in data.TINTUCs
                      where x.ID == id
                      select x;
            return View(tin.Single());
        }
    }
}