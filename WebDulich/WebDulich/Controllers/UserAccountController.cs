﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebDulich.Models;
using System.Web.Security;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Configuration;
using Facebook;
using System.Security.Cryptography;
using System.Text;
using System.Data.Entity;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Net;

namespace WebDulich.Controllers
{


    public class UserAccountController : Controller
    {
        bool invalid = false;

        public static string CreateLostPassword(int PasswordLength)
        {
            string _allowedChars = "abcdefghijk0123456789mnopqrstuvwxyz";
            Random randNum = new Random();
            char[] chars = new char[PasswordLength];
            int allowedCharCount = _allowedChars.Length;

            for (int i = 0; i < PasswordLength; i++)
            {
                chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
            }
            return new string(chars);
        }

        public bool IsValidEmail(string strIn)
        {
            
            invalid = false;
            if (String.IsNullOrEmpty(strIn))
                return false;

            // Use IdnMapping class to convert Unicode domain names.
            try
            {
                strIn = Regex.Replace(strIn, @"(@)(.+)$", this.DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }

            if (invalid)
                return false;

            // Return true if strIn is in valid e-mail format.
            try
            {
                return Regex.IsMatch(strIn,
                      @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                      @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                      RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        private string DomainMapper(Match match)
        {
            // IdnMapping class with default property values.
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            try
            {
                domainName = idn.GetAscii(domainName);
            }
            catch (ArgumentException)
            {
                invalid = true;
            }
            return match.Groups[1].Value + domainName;
        }

        public static string MD5Hash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();


            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));


            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {

                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }

        private Uri RedirectUri
        {
            get
            {
                var uriBuilder = new UriBuilder(Request.Url);
                uriBuilder.Query = null;
                uriBuilder.Fragment = null;
                uriBuilder.Path = Url.Action("FacebookCallBack");
                return uriBuilder.Uri;
            }
        }
        DULICHEntities1 data = new DULICHEntities1();
        
        

        public object FormAuthentication { get; private set; }

        // GET: UserAccount
        public ActionResult Dangky()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Dangky(FormCollection collection, NGUOIDUNG us)
        {
            THONGKEWEB data_tk = data.THONGKEWEBs.SingleOrDefault(n => n.ID ==1);
            var tendn = collection["TENDN"];
            var email = collection["EMAIL"];
            var matkhau = collection["PASS"];
            var matkhaunhaplai = collection["PASS2"];
            var ten = collection["TEN"];
            var sodt = collection["SODT"];
            foreach (var item in data.NGUOIDUNGs)
            {
                if (tendn == item.TENDN)
                {
                    ViewData["Loi1-1"] = "Tên đăng nhập này đã có người sử dụng";
                    return this.Dangky();
                }
                else if (email == item.EMAIL)
                {
                    ViewData["Loi1-2"] = "Email này đã có người sử dụng";
                    return this.Dangky();
                }

            }
            if (String.IsNullOrEmpty(tendn))
            {
                ViewData["Loi1"] = "Tên đăng nhập không được để trống";
            }

            else if (String.IsNullOrEmpty(email))
            {
                ViewData["Loi2"] = "Email không được để trống";
            }

            else if (!IsValidEmail(email))
            {
                ViewData["Loi2-2"] = "Email không đúng định dạng";
            }

            else if (String.IsNullOrEmpty(matkhau))
            {
                ViewData["Loi3"] = "Phải nhập mật khẩu";
            }
            else if (matkhau != matkhaunhaplai)
            {
                ViewData["Loi4"] = "Mật khẩu nhập lại không đúng";
            }
            else if (String.IsNullOrEmpty(ten))
            {
                ViewData["Loi5"] = "Phải nhập họ tên";
            }
            else if (String.IsNullOrEmpty(sodt))
            {
                ViewData["Loi6"] = "Phải nhập số điện thoại";
            }
            else
            {
                matkhau = MD5Hash(matkhau);
                us.TENDN = tendn;
                us.EMAIL = email;
                us.PASS = matkhau;
                us.TEN = ten;
                us.SODT = sodt;
                data.NGUOIDUNGs.Add(us);
                data_tk.TOTAL_USER++;
                data.SaveChanges();
               
                return RedirectToAction("Dangnhap");
            }
            return this.Dangky();
        }

        public ActionResult Dangnhap()
        {
            NGUOIDUNG nd = (NGUOIDUNG)Session["Taikhoan"];
            ADMIN ad = (ADMIN)Session["Admin"];
            NGUOIDUNGFB ndfb = (NGUOIDUNGFB)Session["TaikhoanFB"];
            if (nd != null || ndfb != null)
            {
                return RedirectToAction("Index", "Home");
            }
            if (ad != null)
            {
                return RedirectToAction("Index", "AdminManager");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Dangnhap(FormCollection collection)
        {
            THONGKEWEB data_tk = data.THONGKEWEBs.SingleOrDefault(n => n.ID ==1);
            var tendn = collection["TENDN"];
            var matkhau = collection["PASS"];
            matkhau = MD5Hash(matkhau);
            if (String.IsNullOrEmpty(tendn))
            {
                ViewData["Loi1"] = "Phải nhập tên đăng nhập";
            }
            else if (String.IsNullOrEmpty(matkhau))
            {
                ViewData["Loi2"] = "Phải nhập mật khẩu";
            }
            else
            {
                NGUOIDUNG nd = data.NGUOIDUNGs.SingleOrDefault(n => n.TENDN == tendn && n.PASS == matkhau);
                ADMIN ad = data.ADMINs.SingleOrDefault(n => n.USERNAME == tendn && n.PASSWORD == matkhau);

                if (ad != null)
                {
                    Session["Admin"] = ad;
                    return RedirectToAction("Index", "Home");
                }
                if (nd != null)
                {
                    Session["TaiKhoan"] = nd;
                    data_tk.TOTAL_ONL++;
                    data.SaveChanges();
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ViewBag.Thongbao = "Tên đăng nhập hoặc mật khẩu không đúng";
                }

            }
            return this.Dangnhap();
        }



        public ActionResult LogInFaceBook()
        {
            var fb = new FacebookClient();
            var loginUrl = fb.GetLoginUrl(new
            {
                client_id = ConfigurationManager.AppSettings["FBAppId"],
                client_secret = ConfigurationManager.AppSettings["FBAppSecret"],
                redirect_uri = RedirectUri.AbsoluteUri,
                reponse_type = "code",
                scope = "email",
            });
            return Redirect(loginUrl.AbsoluteUri);
        }

        public ActionResult FacebookCallBack(string code)
        {
            var fb = new FacebookClient();
            THONGKEWEB data_tk = data.THONGKEWEBs.SingleOrDefault(n => n.ID == 1);
            dynamic result = fb.Post("oauth/access_token", new
            {
                client_id = ConfigurationManager.AppSettings["FBAppId"],
                client_secret = ConfigurationManager.AppSettings["FBAppSecret"],
                redirect_uri = RedirectUri.AbsoluteUri,
                code = code
            });
            var accessToken = result.access_token;
            if (!string.IsNullOrEmpty(accessToken))
            {
                fb.AccessToken = accessToken;
                dynamic me = fb.Get("me?fields=first_name,middle_name,last_name,id,email");
                string email = me.email;
                string id = me.id;
                string firstName = me.first_name;
                string middleName = me.middle_name;
                string lastName = me.last_name;
                NGUOIDUNGFB check_us = data.NGUOIDUNGFBs.SingleOrDefault(n => n.ID == id);
                if (check_us == null)
                {
                    NGUOIDUNGFB us = new NGUOIDUNGFB();
                    us.ID = id;
                    us.EMAIL = email;
                    us.TENDN = lastName + " " + firstName;
                    us.TEN = firstName + " " + middleName + " " + lastName;
                    Session["TaiKhoanFB"] = us;
                    data.NGUOIDUNGFBs.Add(us);
                    data_tk.TOTAL_ONL++;
                    data.SaveChanges();
                    
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    data_tk.TOTAL_ONL++;
                    data.SaveChanges();
                    Session["TaiKhoanFB"] = check_us;
                    return RedirectToAction("Index", "Home");
                }


            }
            return RedirectToAction("/");
        } 

        public ActionResult Dangxuat()
        {
            THONGKEWEB data_tk = data.THONGKEWEBs.SingleOrDefault(n => n.ID == 1);
            NGUOIDUNG user = (NGUOIDUNG)Session["TaiKhoan"];
            ADMIN ad = (ADMIN)Session["Admin"];
            NGUOIDUNGFB userfb = (NGUOIDUNGFB)Session["TaikhoanFB"];

            if (user != null)
            {
                if(data_tk.TOTAL_ONL>0)
                    data_tk.TOTAL_ONL--;
                data.SaveChanges();
                Session.Remove("taikhoan");
            }
            if (ad != null)
            {
                Session.Remove("Admin");
            }
            if(userfb != null)
            {
                if (data_tk.TOTAL_ONL > 0)
                    data_tk.TOTAL_ONL--;
                data.SaveChanges();
                Session.Remove("taikhoanFB");
                
            }

            return Redirect("/");
        }


        public ActionResult Doimatkhau()
        { 
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Doimatkhau(FormCollection collection, NGUOIDUNG us)
        {
            NGUOIDUNG user = (NGUOIDUNG)Session["TaiKhoan"];

            var matkhaucu = collection["OldPass"];
            var matkhaumoi = collection["NewPass"];
            var matkhauxacnhan = collection["ConfirmPass"];
            matkhaucu = MD5Hash(matkhaucu);
            matkhaumoi = MD5Hash(matkhaumoi);
            matkhauxacnhan = MD5Hash(matkhauxacnhan);

            if (matkhaucu != user.PASS)
            {
                ViewData["Loi1"] = "Mật khẩu không đúng";
            }
            else if (matkhaumoi == matkhaucu)
            {
                ViewData["Loi2"] = "Mật khẩu mới phải khác mật khẩu cũ";
            }
            else if (matkhaumoi != matkhauxacnhan)
            {
                ViewData["Loi3"] = "Mật khẩu xác nhận không trùng khớp";
            }

            else
            {
                if (TryUpdateModel(us, "", new string[] { "TEN, TENDN, EMAIL, PASS, SODT" }))
                {
                    if (ModelState.IsValid)
                    {
                        us.ID = user.ID;
                        us.PASS = matkhaumoi;
                        us.TEN = user.TEN;
                        us.TENDN = user.TENDN;
                        us.EMAIL = user.EMAIL;
                        us.SODT = user.SODT;
                        data.Entry(us).State = EntityState.Modified;
                        data.SaveChanges();
                        return RedirectToAction("Thongtin");
                    }
                }
            }
            return this.Doimatkhau();
        }


        public ActionResult Quenmatkhau()
        {
            return View();
        }
        

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Quenmatkhau(FormCollection collection)
        {
            NGUOIDUNG user = (NGUOIDUNG)Session["TaiKhoan"];

            var email = collection["Email"];
             int check = 0;
            foreach(var item in data.NGUOIDUNGs)
            {
                if (email == item.EMAIL)
                {
                    check = 1;
                    break;
                }
                else
                    check = 0;
            }
            
            if (String.IsNullOrEmpty(email))
            {
                ViewData["Loi1"] = "Phải nhập email";
            }
            else if (!IsValidEmail(email))
            {
                ViewData["Loi2"] = "Email không đúng định dạng";
            }
            else if (check !=1)
            {
                ViewData["Loi3"] = "Bạn nhập sai email đăng ký";
            }
            else
            {
                user = data.NGUOIDUNGs.SingleOrDefault(n => n.EMAIL == email);
                string pass = "";
                pass = CreateLostPassword(7);
                
                MailMessage mm = new MailMessage("kpdulichvn@gmail.com", email);
                mm.Subject = "Password Recovery";
                mm.Body = string.Format("Hi {0},<br /><br />Pass mới của bạn là {1}.<br /><br />Xin vui lòng đổi lại để thuận tiện cho việc sử dụng. Cảm ơn", user.TENDN, pass);
                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                NetworkCredential NetworkCred = new NetworkCredential();
                NetworkCred.UserName = "kpdulichvn@gmail.com";
                NetworkCred.Password = "Loc7845129";
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                smtp.Send(mm);
                pass = MD5Hash(pass);
                user.PASS = pass;
                data.SaveChanges();
                return RedirectToAction("Dangnhap");
            }
            return this.Dangnhap();
        }


        public ActionResult Thongtin()
        {
            NGUOIDUNG nd = (NGUOIDUNG)Session["Taikhoan"];
            NGUOIDUNGFB ndfb = (NGUOIDUNGFB)Session["TaikhoanFB"];
            if (nd!=null || ndfb!=null)
                return View(data.NGUOIDUNGs.ToList());
            else
                return RedirectToAction("Index", "Home");
        }

        public ActionResult Vietbai()
        {
            NGUOIDUNG nd = (NGUOIDUNG)Session["Taikhoan"];
            NGUOIDUNGFB ndfb = (NGUOIDUNGFB)Session["TaikhoanFB"];
            if (nd != null || ndfb != null)
            {
                var listCategory = from s in data.LOAITINs
                                   select new { s.ID, s.TENLOAI };

                var list = listCategory.ToList().Select(c => new SelectListItem
                {
                    Text = c.TENLOAI,
                    Value = c.ID.ToString(),
                    Selected = (c.ID == 1)
                }).ToList();

                ViewBag.listCate = list;
                return View();
            }
            else
                return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Vietbai([Bind(Include = "TIEUDE, MOTA, NOIDUNG, ID_LOAI, ID_USER,ID_USERFB")] BAIVIET bv, HttpPostedFileBase hinh, FormCollection collection)
        {
            THONGKEWEB data_tk = data.THONGKEWEBs.SingleOrDefault(n => n.ID == 1);
            var noidung = collection["noidung"];
            NGUOIDUNG nd = (NGUOIDUNG)Session["TaiKhoan"];
            NGUOIDUNGFB ndfb = (NGUOIDUNGFB)Session["TaiKhoanFB"];
            if (nd != null)
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        if (hinh.ContentLength > 0)
                        {
                            var pathFile = "/Content/Images/";
                            var fileName = Path.GetFileName(hinh.FileName);

                            bool exists = Directory.Exists(Server.MapPath(pathFile));
                            if (!exists)
                            {
                                Directory.CreateDirectory(Server.MapPath(pathFile));
                            }
                            var path = Path.Combine(Server.MapPath(pathFile), fileName);
                            hinh.SaveAs(path);

                            bv.ANHBIA = pathFile + fileName;
                            bv.NOIDUNG = noidung;
                        }
                        bv.ID_USER = nd.ID;
                        bv.ID_USERFB = null;
                        DateTime time= DateTime.Now;
                        bv.NGAYDANG = time;
                        data.BAIVIETs.Add(bv);
                        data_tk.TOTAL_BAIVIET++;
                        data.SaveChanges();   
                    }
                }

                catch (RetryLimitExceededException)
                {
                    ModelState.AddModelError("", "Có lỗi xảy ra");
                }
                return RedirectToAction("Thongtin");
            }
            else if (ndfb != null)
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        if (hinh.ContentLength > 0)
                        {
                            var pathFile = "/Content/Images/";
                            var fileName = Path.GetFileName(hinh.FileName);

                            bool exists = Directory.Exists(Server.MapPath(pathFile));
                            if (!exists)
                            {
                                Directory.CreateDirectory(Server.MapPath(pathFile));
                            }
                            var path = Path.Combine(Server.MapPath(pathFile), fileName);
                            hinh.SaveAs(path);

                            bv.ANHBIA = pathFile + fileName;
                            bv.NOIDUNG = noidung;
                        }
                        bv.ID_USERFB = ndfb.ID;
                        bv.ID_USER = null;
                        DateTime time = DateTime.Now;
                        bv.NGAYDANG = time;
                        data.BAIVIETs.Add(bv);
                        data_tk.TOTAL_BAIVIET++;
                        data.SaveChanges();
                    }
                }
                catch (RetryLimitExceededException)
                {
                    ModelState.AddModelError("", "Có lỗi xảy ra");
                }
                return RedirectToAction("Thongtin");
            
            }
            else
                return RedirectToAction("Index", "Home");
        }

        public ActionResult Noiquy()
        {
            return View();
        }

        public ActionResult Chinhsach()
        {
            return View();
        }

    }
}